const { Router } = require('express');
const router = Router();
const scheme = require('../models/usersPosts');

router.get('/', async (req, res) => {
    try {
        const posts = await scheme.find({});
        res.json(posts);
    } catch (error) {
        res.json({ error });
    }
});

module.exports = router;