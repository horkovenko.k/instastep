const { Router } = require('express');
const router = Router();
const scheme = require('../models/commentsMainPost');

router.get('/', async (req, res) => {
    try {
        const posts = await scheme.find({});
        res.json(posts);
    } catch (error) {
        res.json({ error });
    }
});

router.post('/', async (req, res) => {
    const post = new scheme({
        class: req.body.class,
        avatar: req.body.avatar,
        text: req.body.text,
        name: req.body.name
    });
    try {
        const savedPosts = await post.save();
        res.json(savedPosts);
    } catch (error) {
        res.json({ message: error });
    }
});

module.exports = router;