const mongoose = require("mongoose");

const Schema = mongoose.Schema;

let usersPosts = new Schema({

    nickname: String,
    id: String,
    path: String

}, {collection: 'usersPosts'} );

module.exports = mongoose.model("usersPosts", usersPosts);