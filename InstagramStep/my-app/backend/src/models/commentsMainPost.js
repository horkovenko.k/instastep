const mongoose = require("mongoose");

const Schema = mongoose.Schema;

let commentsMainPost = new Schema({

    class: String,
    avatar: String,
    text: String,
    name: String

}, {collection: 'commentsMainPost'} );

module.exports = mongoose.model("commentsMainPost", commentsMainPost);