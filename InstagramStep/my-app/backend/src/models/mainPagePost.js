const mongoose = require("mongoose");

const Schema = mongoose.Schema;

let mainPagePost = new Schema({

    id: String,
    avatar: String,
    img: String,
    name: String

}, {collection: 'mainPagePost'} );

module.exports = mongoose.model("mainPagePost", mainPagePost);