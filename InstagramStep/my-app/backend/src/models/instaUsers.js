const mongoose = require("mongoose");

const Schema = mongoose.Schema;

let instaUser = new Schema({

    name: String,
    nickname: String,
    avatar: String

}, {collection: 'instaUsers'} );

module.exports = mongoose.model("instaUser", instaUser);