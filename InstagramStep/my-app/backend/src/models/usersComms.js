const mongoose = require("mongoose");

const Schema = mongoose.Schema;

let usersComms = new Schema({

    class: String,
    ava: String,
    text: String,
    author: String

}, {collection: 'usersComms'} );

module.exports = mongoose.model("usersComms", usersComms);