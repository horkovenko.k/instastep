const mongoose = require("mongoose");

const Schema = mongoose.Schema;

let test = new Schema({
    name: {
        type: String
    }
});

module.exports = mongoose.model("test", test);