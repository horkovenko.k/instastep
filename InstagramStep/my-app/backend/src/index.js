const express = require("express");
const router = express.Router();
const app = express();
const cors = require("cors");
const PORT = 4000;
const mongoose = require("mongoose");
const instaUserRoute = require("./routes/instaUsersRoute");
const commentsMainPostRoute = require("./routes/commentsMainPostRoute");
const mainPagePostRoute = require("./routes/mainPagePostRoute");
const usersCommsRoute = require("./routes/usersCommsRoute");
const usersPostsRoute = require("./routes/usersPostsRoute");
const jsonParser = express.json();
const Schema = mongoose.Schema;
const postSchema = require('./models/commentsMainPost')
let MongoClient = require('mongodb').MongoClient;

// import commentsMainPost from "./models/commentsMainPost"

app.use(cors());

let uri = 'mongodb+srv://u:astana12@test.uc7lb.mongodb.net/Instagram';
let loc = 'mongodb://127.0.0.1:27017/details';

mongoose.connect(uri, {
    useNewUrlParser: true
});

const connection = mongoose.connection;

connection.once("open", function() {
    console.log("Connection with MongoDB was successful");
});

app.use("/", router);
app.use("/instaUsers", instaUserRoute);
app.use("/commentsMainPost", commentsMainPostRoute);
app.use("/mainPagePost", mainPagePostRoute);
app.use("/usersComms", usersCommsRoute);
app.use("/usersPosts", usersPostsRoute);

app.listen(PORT, function() {
    console.log("Server is running on Port: " + PORT);
});