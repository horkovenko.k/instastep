import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {composeWithDevTools} from 'redux-devtools-extension';
import {reducer as productsReducer,
    POST_NAME as postsModule,
    COMMENT_NAME as commentsModule} from './action'

const rootReducer = combineReducers({
    [postsModule]: productsReducer,
    [commentsModule]: productsReducer,
});

const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));

export default store;