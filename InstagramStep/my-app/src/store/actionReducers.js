import { GET_POSTS, GET_COMMENTS, GET_USERS, GET_USERS_POSTS, GET_USERS_COMMENTS } from "./action";

const setPosts = (payload) => ({
    type: GET_POSTS,
    payload
});

const setComments = (payload) => ({
    type: GET_COMMENTS,
    payload
});

const setUsers = (payload) => ({
    type: GET_USERS,
    payload
});

const setUsersPosts = (payload) => ({
    type: GET_USERS_POSTS,
    payload
});

const setUsersComments = (payload) => ({
    type: GET_USERS_COMMENTS,
    payload
});

export const getPosts = () => async dispatch => {
    let response = await fetch('http://localhost:4000/mainPagePost');
    let answer = await response.json();
    dispatch(setPosts(answer));
};

export const getComments = () => async dispatch => {
    let response = await fetch('http://localhost:4000/commentsMainPost');
    let answer = await response.json();
    dispatch(setComments(answer));
};

export const getUsers = () => async dispatch => {
    let response = await fetch('./instaUsers.json');
    let answer = await response.json();
    dispatch(setUsers(answer));
};

export const getUsersPosts = () => async dispatch => {
    let response = await fetch('./usersPosts.json');
    let answer = await response.json();
    dispatch(setUsersPosts(answer));
};

export const getUsersComments = () => async dispatch => {
    let response = await fetch('./usersComms.json');
    let answer = await response.json();
    dispatch(setUsersComments(answer));
};

