export let GET_POSTS = "GET_POSTS";
export let GET_COMMENTS = "GET_COMMENTS";
export let GET_USERS = "GET_USERS";
export let GET_USERS_POSTS = "GET_USERS_POSTS";
export let GET_USERS_COMMENTS = "GET_USERS_COMMENTS";

export let POST_NAME = "posts";
export let COMMENT_NAME = "comments";
export let USERS_NAME = "users";
export let USERS_COMMENT_NAME = "usersComments";
export let USERS_POSTS_NAME = "usersPosts";

export const selectPostsList = state => state[POST_NAME].posts;
export const selectCommentsList = state => state[COMMENT_NAME].comments;
export const selectUsersList = state => state[USERS_NAME].comments;
export const selectUsersCommentsList = state => state[USERS_COMMENT_NAME].comments;
export const selectUsersPostsList = state => state[USERS_POSTS_NAME].comments;

let initialState = {
    posts: [],
    comments: [],
    users: [],
    userComments: [],
    userPosts: []
};

export function reducer(state = initialState, {type, payload}) {
    switch (type) {
        case GET_POSTS:
            return {
                ...state,
                posts: payload
            };

        case GET_COMMENTS:
            return {
                ...state,
                comments: payload
            };

        case GET_USERS:
            return {
                ...state,
                comments: payload
            };

        case GET_USERS_POSTS:
            return {
                ...state,
                comments: payload
            };

        case GET_USERS_COMMENTS:
            return {
                ...state,
                comments: payload
            };

        default:
            return state;
    }
}

