import React, {useEffect, useRef, useState} from 'react';
import UserPostsPage from "../UserPostsPage/UserPostsPage";
import MainPage from "../MainPage/MainPage";

import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import Post from "../MainPage/Post/Post";
import Comment from "../MainPage/Post/Comment/Comment";
import UsersPhoto from "../UserPostsPage/UsersPhoto/UsersPhoto";
import CommentUserPost from "../UserPostsPage/CommentUserPost/CommentUserPost";

const RoutesMain = () => {
    const [users, setUsers] = useState([]);

    const [postList, setPostList] = useState([]);
    const [comList, setComList] = useState([]);

    let subBtn = useRef(null);

    useEffect(() => {
        async function arrGet() {
            let response = await fetch('./usersPosts.json');
            let posts = await response.json();
            setPostList(posts);

            let response2 = await fetch('./usersComms.json');
            let comments = await response2.json();
            setComList(comments);
        }arrGet();


    }, []);

    useEffect(() => {
        async function arrGet() {
            let response = await fetch('./instaUsers.json');
            let posts = await response.json();

            setUsers(posts);
        }arrGet();

    }, []);

    return (
        <Switch>
            <Route exact path='/' render={() => <MainPage/>} />

            {users.map((item, index) =>
                <Route exact path={`/${item.nickname}`}  key={index} render={() => <UserPostsPage key={index} mainAvatar={item.avatar} name={item.name} nickname={item.nickname}
                    photos={postList.map((it, i) => (item.nickname === it.nickname) ?
                        <UsersPhoto key={i} id={it.id} path={it.path} nick={item.nickname} ava={item.avatar}
                                    coms={ comList.map((elem, id) => (i == elem.class) ?
                                        <CommentUserPost key={id} avatar={elem.ava} text={elem.text} author={elem.author} /> : '' )}/> : '')}
                />}/>)}

            {/*{postList.map((item, index) =>*/}
            {/*    <UsersPhoto key={index} id={item.id} path={item.path}*/}
            {/*                coms={ comList.map((elem, id) => (index == elem.class) ?*/}
            {/*                    <CommentUserPost key={id} avatar={elem.ava} text={elem.text} author={elem.author} /> : '' )}/>)}*/}

        </Switch>
    );
};

export default RoutesMain;