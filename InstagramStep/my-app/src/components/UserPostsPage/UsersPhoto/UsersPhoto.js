import React, {useState, useRef, useEffect} from 'react';
import "./UsersPhoto.scss"
import img from "../ava.jpg"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHeart } from '@fortawesome/free-solid-svg-icons'
import { faComment } from '@fortawesome/free-solid-svg-icons'


const UsersPhoto = (props) => {
    const {coms, path, ava, nick} = props;

    const [isOpen, setIsOpen] = useState(false);
    const [commentsNumber, setCommentsNumber] = useState(0);
    let comment = useRef(null);
    let currentPhoto = useRef(null);




    useEffect(() => {
        setCommentsNumber(comment.current.children.length);
    },[]);

    let closeModal = (e) => {
        if (e.target.classList.contains('modal-comments')) {
            setIsOpen(false);
        }
    };

    let openModal = () => {
        setIsOpen(true);

    };

    return (
        <div>
        <div className={"main-content"} onClick={openModal}>
            <img ref={currentPhoto} src={path} alt="ava" className={"post-img"}/>
            <div className={"curtain"}>
                <div className="likes-counter">
                    <FontAwesomeIcon icon={faHeart} className={'fa-content'}/>
                    <span className={'fa-content'}>12</span>
                </div>
                <div className="comments-counter">
                    <FontAwesomeIcon icon={faComment} className={'fa-content'}/>
                    <span className={'fa-content'}>{commentsNumber}</span>
                </div>
            </div>


        </div>
            <section className={`modal-comments ${isOpen ? '' : 'dn'}`} onClick={closeModal}>

                <div className={"photo-nest"}>
                    <img src={path} alt="post" className={"user-post"}/>
                    <div className={"post-comments"}>
                        <div className={"user"}>
                            <img src={ava} alt="logo" className={"user-id"}/>
                            <p className={"user-name"} data-testid="nick">{nick}</p>
                        </div>
                        <div ref={comment} className="comments-photo">

                            {coms}

                        </div>
                    </div>
                </div>
            </section>
        </div>
    );
};

export default UsersPhoto;