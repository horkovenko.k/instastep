import '@testing-library/jest-dom';
import React from "react";
import UsersPhoto from "./UsersPhoto";
import { render, screen } from "@testing-library/react";
import userEvent  from "@testing-library/user-event";
import {getByTestId} from "@testing-library/dom";

describe("Unit testing UsersPhoto", () => {

    test("smoke test UsersPhoto", () => {
        render(<UsersPhoto />);
    });

    test('First test', () => {
        const ancestor = getByTestId('nick');

        expect(ancestor).not.toBeEmpty();

        expect(ancestor).toHaveClass('user-name');
    });

});