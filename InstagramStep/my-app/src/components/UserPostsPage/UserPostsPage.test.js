import '@testing-library/jest-dom';
import React from "react";
import UserPostsPage from "./UserPostsPage";
import { render, screen } from "@testing-library/react";
import userEvent  from "@testing-library/user-event";
import {getByTestId} from "@testing-library/dom";

describe("Unit testing UserPostsPage", () => {

    test("smoke test UserPostsPage", () => {
        render(<UserPostsPage />);
    });

    test('First test', () => {
        const descendant = getByTestId('subs-btn');
        const ancestor = getByTestId('subs');

        expect(ancestor).toContainElement(descendant);

        expect(ancestor).toHaveClass('sub-block');
        expect(descendant).toHaveClass('subuser-btn')
    });

});