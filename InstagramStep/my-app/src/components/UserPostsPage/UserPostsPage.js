import React, {useEffect, useRef, useState} from 'react';
import "./UserPostsPage.scss";
import img1 from "./ava.jpg";
import img2 from "./ava3.jpg";
import img3 from "./do1d.jpg";
import UsersPhoto from "./UsersPhoto/UsersPhoto";
import Post from "../MainPage/Post/Post";
import CommentUserPost from "./CommentUserPost/CommentUserPost";

const UserPostsPage = (props) => {
    const {mainAvatar, name, nickname, photos} = props;

    const [postList, setPostList] = useState([]);
    const [comList, setComList] = useState([]);

    const [nick, setNickname] = useState('');

    let subBtn = useRef(null);

    useEffect(() => {
      /*  async function arrGet() {
            let response = await fetch('./usersPosts.json');
            let posts = await response.json();
            setPostList(posts);

            let response2 = await fetch('./usersComms.json');
            let comments = await response2.json();
            setComList(comments);
        }arrGet();*/

        if (localStorage.getItem(`follower ${nickname}`)) {
            subBtn.current.textContent = 'Unsubscribe';
        }

    }, []);

    let subWatcher = () => {
         if (subBtn.current.textContent === "Subscribe") {
             subBtn.current.textContent = "Unsubscribe"
        } else {
             subBtn.current.textContent = "Subscribe"
        }

        if (localStorage.getItem(`follower ${nickname}`)) {
            localStorage.removeItem(`follower ${nickname}`);
        } else {
            localStorage.setItem(`follower ${nickname}`, nickname);
        }

    };



    return (
        <div className={"user-content"}>

            <div className={"info-block"}>
                <img src={mainAvatar} alt="avatar" className={"main-avatar"}/>

                <div className="sub-block"   data-testid="subs">
                    <div className={"nick-block"}>
                        <h3 className={'user-nickname'}>{nickname}</h3>
                        <button ref={subBtn} className={'subuser-btn'} onClick={subWatcher} data-testid="subs-btn">Subscribe</button>
                    </div>

                    <p className={"user-name"}>{name}</p>
                </div>
            </div>
            <div className={'hr'}> </div>

            <div className={"user-photos"}>

                {photos}
                {/*{postList.map((item, index) =>*/}
                {/*    <UsersPhoto key={index} id={item.id} path={item.path}*/}
                {/*          coms={ comList.map((elem, id) => (index == elem.class) ?*/}
                {/*              <CommentUserPost key={id} avatar={elem.ava} text={elem.text} author={elem.author} /> : '' )}/>)}*/}

            </div>

        </div>
    );
};

export default UserPostsPage;