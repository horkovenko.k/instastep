import React from 'react';
import "./CommentUserPost.scss";
import img from "../ava.jpg";

const CommentUserPost = (props) => {
    const {avatar, author, text} = props;

    return (
        <div className={"f-comment"}>
            <img src={avatar} alt="logo" className={"user-ava"}/>
            <p className={"comment-body"}><span className={"comment-author"}>{author}</span>{text}</p>
        </div>
    );
};

export default CommentUserPost;