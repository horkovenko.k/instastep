import React, {useEffect, useState, useRef, Children} from 'react';
import "./Post.scss"
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faHeart} from "@fortawesome/free-solid-svg-icons";
import Comment from "./Comment/Comment";
import PropTypes from 'prop-types';
import {selectPostsList, selectCommentsList} from "../../../store/action";
import {getPosts, getComments} from "../../../store/actionReducers";
import {connect} from "react-redux";
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import {faCheckDouble} from '@fortawesome/free-solid-svg-icons'
import axios from "axios";


const Post = (props) => {
    const {path, name, bgi, com, postId} = props;

    const comBlock = useRef(null);
    const comBt = useRef(null);
    const valuePost = useRef(null);

    const [ico, setIco] = useState(false);
    const [sm, setSm] = useState(true);
    const [passValue, setPassValue] = useState('');

    useEffect(() => {
        let lng = comBlock.current.children.length;

        if (lng <= 2) {
            setSm(false);
        } else if (lng > 2) {
            setSm(true);
            for (let i = 0; i < lng - 2; i++) {
                comBlock.current.children[i].classList.add('dn');
            }
        }

    }, [com, comBlock]);

    useEffect(() => {
        for (let i = 0; i < localStorage.length; i++) {
            if (localStorage.key(i).includes('liked' + name)) {
                setIco(true)
            }
        }

    }, []);


    function moreShow() {
        let lng = comBlock.current.children.length;
        for (let i = 0; i < lng; i++) {
            comBlock.current.children[i].classList.remove('dn')
        }
    }

    function likeIt(path) {
        setIco(!ico);
        if (localStorage.getItem('liked' + path)) {
            localStorage.removeItem('liked' + path);
        } else {
            localStorage.setItem('liked' + path, path);
        }
    }

    let addComment = (id, val) => {
        // axios.post(`http://localhost:4000/commentsMainPost`, null, { params: {
        //         class: id,
        //         avatar: "./img/Post/pin.png",
        //         text: passValue,
        //         name: "Konstantin Horkovenko"
        //     }})
        //     .then(response => {
        //         console.log(response.status)
        //         setPassValue('')
        //     })
        //     .catch(err => console.warn(err));

        fetch('http://localhost:4000/commentsMainPost', {
            method: 'POST',
            body: JSON.stringify({
                class: id,
                avatar: "./img/Post/pin.png",
                text: passValue,
                name: "Konstantin Horkovenko"
            })
        })
            .then((response) => response.json())
            .then((json) => console.log(json))

        setPassValue('')
    }

    let sendValue = (e) => {
        setPassValue(e.target.value)
    }

    return (

        <div className="post-block">
            <header className={'post-author'}>
                <div className={'post-author__emblem'}>
                    <img src={path} alt="post photo" className={'post-photo'}/>
                    <h3 className={'author-name'} >{name}</h3>
                </div>

                <FontAwesomeIcon  icon={faHeart} className={`like-ico ${ico ? 'liked' : ''}`} onClick={likeIt.bind(this, name)} />
            </header>
            <img src={bgi} alt="image" className={"bg-image"} onDoubleClick={likeIt.bind(this, name)}/>


            <div className={"add-comment"}  data-testid="comment">
                <TextField innerRef={valuePost} value={passValue} onChange={sendValue} id="outlined-basic" label="Your impressions" variant="outlined" className={"comment-field"}/>
                {/*<input type="text" className={"comment-field"}/>*/}
                <Button data-testid="comment-btn" variant="contained" className={"btn-comment"} onClick={addComment.bind(this, postId, valuePost.current === null ? '' : valuePost.current.querySelectorAll('input')[postId] )} >
                    <FontAwesomeIcon icon={faCheckDouble} className={'cm'}/>
                </Button>
            </div>

            <div ref={comBlock} className={"comment-block"}>

                {com}

                {/*<Button ref={comBt} onClick={moreShow} className={"btn-all"}>*/}
                <Button variant="contained" color="primary" ref={comBt} onClick={moreShow} className={`btn-all ${sm ? '' : 'dn'}`}>
                    Show more
                </Button>
            </div>
            
        </div>
    );
};

const mapStateToProps = (state) => ({
    posts: selectPostsList(state),
    comments: selectCommentsList(state)
});


export default connect(mapStateToProps, {getPosts, getComments})(Post);