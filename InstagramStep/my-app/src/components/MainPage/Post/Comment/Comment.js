import React from 'react';
import "./Comment.scss";

const Comment = (props) => {
    const {img, txt, name} = props;

    return (
        <div className={"comment-border"}>
            <header className={"comment-header"}>
                <img src={img} alt="avatar" className={"comment-avatar"}/>
                <h3 className={"header-name"}>{name}</h3>
            </header>
            <p className="comment-text">
                {txt}
            </p>
        </div>
    );
};

export default Comment;