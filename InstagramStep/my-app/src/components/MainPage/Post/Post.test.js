import '@testing-library/jest-dom';
import React from "react";
import Post from "./Post";
import { render, screen } from "@testing-library/react";
import userEvent  from "@testing-library/user-event";
import {getByTestId} from "@testing-library/dom";

describe("Unit testing Post", () => {

    test("smoke test Post", () => {
        render(<Post />);
    });

    test('First test', () => {
        const descendant = getByTestId('comment-btn');
        const ancestor = getByTestId('comment');

        expect(ancestor).toContainElement(descendant);

        expect(ancestor).toHaveClass('add-comment');
        expect(descendant).toHaveClass('btn-comment');
    });

});