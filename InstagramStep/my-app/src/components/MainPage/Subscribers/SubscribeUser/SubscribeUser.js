import React, {useRef} from 'react';
import "./SubscribeUser.scss";
import img from "../ava.jpg";
import {Redirect, Link} from "react-router-dom"

const SubscribeUser = (props) => {
    let {name, ava} = props;
    const btn = useRef(null);


    function changeLbl(name) {
        // return btn.current.textContent === "Subscribe" ?
        //     btn.current.textContent = "Unsubscribe" :
        //     btn.current.textContent = "Subscribe";

        if (btn.current.textContent === "Subscribe") {
            btn.current.textContent = "Unsubscribe"
        } else {
            btn.current.textContent = "Subscribe"
        }

        if (localStorage.getItem(`follower ${name}`)) {
            localStorage.removeItem(`follower ${name}`);
        } else {
            localStorage.setItem(`follower ${name}`, name);
        }
    }

    let toPage = (e) => {
        return <Redirect to={`/${name}`}/>
    };

    return (
        <div className={'subBtn-aligner'}>
        <Link to={`/${name}`} style={{textDecoration: 'none', color: 'black'}}>

            <div className={"subscriber"} onClick={toPage.bind(this, name)}>
                <img src={ava} alt="logo" className={"sub-ava"}/>
                <p className={"sub-name"}>{name}</p>
            </div>
        </Link>
            <p className={'sub-btn'} onClick={changeLbl.bind(this, name)} ref={btn}>Subscribe</p>
        </div>
    );
};

export default SubscribeUser;