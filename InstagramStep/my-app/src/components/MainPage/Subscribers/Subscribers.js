import React, {useEffect, useState} from 'react';
import "./Subscribers.scss";
import img from "../Followers/ava.jpg";
import SubscribeUser from "../Subscribers/SubscribeUser/SubscribeUser";
import FollowUser from "../Followers/FollowUser/FollowUser";
import axios from "axios";
const Subscribers = () => {

    const [user, setUser] = useState([]);


    useEffect(() => {
        /*async function getUsers() {
            let response = await fetch('./instaUsers.json');
            let users = await response.json();
            setUser(users);
            // users.map(item => localStorage.setItem(`follower ${item.name}`, item.name));
        } getUsers();*/

        axios.get("http://localhost:4000/instaUsers").then(res => {
            setUser(res.data);
        });
    }, []);

    function isHereSubs(i) {
        if (localStorage.getItem(`follower ${user[i].nickname}`)) {
            return true;
        }
    }


    return (
        <div className={"sub-content"}>
            <p className={"recomend"} data-testid="caption">Recommended</p>

            <div className="infinity-block-subs">

                {user.map((item, index) => !isHereSubs(index) ?
                    <SubscribeUser key={index}  ava={item.avatar} name={item.nickname}/> : ''
                )}
            </div>
        </div>
    );
};

export default Subscribers;