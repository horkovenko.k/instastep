import '@testing-library/jest-dom';
import React from "react";
import Subscribers from "./Subscribers";
import { render, screen } from "@testing-library/react";
import userEvent  from "@testing-library/user-event";
import {getByTestId} from "@testing-library/dom";

describe("Unit testing MainPage", () => {

    test("smoke test MainPage", () => {
        render(<Subscribers />);
    });

    test('First test', () => {
        expect(screen.getByTestId('caption')).toHaveTextContent('Recommended');
    });

});