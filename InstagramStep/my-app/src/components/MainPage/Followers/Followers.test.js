import '@testing-library/jest-dom';
import React from "react";
import Followers from "./Followers";
import { render, screen } from "@testing-library/react";
import userEvent  from "@testing-library/user-event";
import {getByTestId} from "@testing-library/dom";

describe("Unit testing MainPage", () => {

    test("smoke test MainPage", () => {
        render(<Followers />);
    });

    test('First test', () => {
        expect(screen.getByTestId('name')).toHaveTextContent('Konstantin');
        expect(screen.getByTestId('nickname')).toHaveTextContent('Konstantin Horkovenko');
        expect(screen.getByTestId('caption')).toHaveTextContent('Tales');

    });

});