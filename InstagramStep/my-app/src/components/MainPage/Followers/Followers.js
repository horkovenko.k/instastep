import React, {useEffect, useState} from 'react';
import "./Followers.scss"
import img from "./ava.jpg";
import FollowUser from "./FollowUser/FollowUser";
import InfiniteScroll from 'react-infinite-scroller';
import UserPostsPage from "../../UserPostsPage/UserPostsPage";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import Post from "../Post/Post";
import axios from "axios";

const Followers = (props) => {
    const [user, setUser] = useState([]);


    useEffect(() => {
        /*async function getUsers() {
            let response = await fetch('./instaUsers.json');
            let users = await response.json();
            setUser(users);

        } getUsers();*/
        axios.get("http://localhost:4000/instaUsers").then(res => {
            setUser(res.data);
        });
    }, []);

    function isHereFollower(i) {
        if (localStorage.getItem(`follower ${user[i].nickname}`)) {
            return true;
        }
    }


    return (
        <div className={"follower-content"}>
            <div className={"user-block"}>
                <img src={img} alt="avatar" className={"user-avatar"}/>

                <div className={"identity"}>
                    <p className={"nickname"}  data-testid="nickname">Konstantin Horkovenko</p>
                    <p className={"name"}  data-testid="name">Konstantin</p>
                </div>
            </div>

            <div className="followers-block">
                <div className={"nav-follow"}>
                    <span className={"tales"} data-testid="caption">Tales</span>
                    <span className={"view"}>View all</span>
                </div>

                <div className="infinity-block">
                    {user.map((item, index) => isHereFollower(index) ?
                            <FollowUser  key={index}  ava={item.avatar} name={item.nickname}/>
                        : ''
                    )}
                </div>
            </div>


        </div>
    );
};

export default Followers;