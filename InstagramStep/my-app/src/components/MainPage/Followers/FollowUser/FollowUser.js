import React from 'react';
import "./FollowUser.scss"
import img from "../ava.jpg";
import {Redirect, Link} from "react-router-dom"

const FollowUser = (props) => {
    const {name, ava} = props;

    let toPage = () => {
        return <Redirect to={`/${name}`}/>
    };

    return (
        <Link to={`/${name}`} style={{textDecoration: 'none', color: 'black'}}>
            <div className={"align-block"} onClick={toPage.bind(this, name)}>
                <img src={ava} alt="logo" className={"follow-ava"}/>
                <p className={"follow-name"}>{name}</p>

            </div>
        </Link>
    );
};

export default FollowUser;