import '@testing-library/jest-dom';
import React from "react";
import MainPage from "./MainPage";
import { render, screen } from "@testing-library/react";
import userEvent  from "@testing-library/user-event";
import {getByTestId} from "@testing-library/dom";

describe("Unit testing MainPage", () => {

    test("smoke test MainPage", () => {
        render(<MainPage />);
    });

    test('First test', () => {
        const descendant1 = getByTestId('descendant1')
        const descendant2 = getByTestId('descendant2')
        const ancestor = getByTestId('users-wrapper')

        expect(ancestor).not.toContainElement(descendant1)
        expect(ancestor).not.toContainElement(descendant2)

        expect(ancestor).toHaveClass('users-wrapper');
    });

});