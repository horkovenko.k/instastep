import React, {useEffect, useState} from 'react';
import Post from "./Post/Post";
import {connect} from "react-redux"
import Followers from "./Followers/Followers"
import Subscribers from "./Subscribers/Subscribers";
import "./MainPage.scss";
import InfiniteScroll from 'react-infinite-scroll-component';
import Comment from "./Post/Comment/Comment";
import {getPosts, getComments} from "../../store/actionReducers";
import {selectPostsList, selectCommentsList} from "../../store/action";
import axios from "axios";

const MainPage = (props) => {
    const {getPosts, getComments, posts, comments} = props;



    useEffect(() => {

        axios.get("http://localhost:4000/mainPagePost").then(res => {
            setPostList(res.data);
            setLoadList([res.data[0], res.data[1], res.data[2]]);
        });

        axios.get("http://localhost:4000/commentsMainPost").then(res => {
            setComList(res.data);
        });

        /*getComments();
        getPosts();*/

    }, []);

    const [postList, setPostList] = useState([]);
    const [loadList, setLoadList] = useState([]);
    const [comList, setComList] = useState([]);


    let c = 3;

    function inc() {
        return c * 2;
    }



    let fetchMoreData = () => {
        setTimeout(() => {
            setLoadList(
                postList.slice(0, loadList.length + c)
            );
        }, 1000);
        inc();
    };

    return (
        <div className={"main-aligner"}>

            <div className={"main-page-content"}>
                <InfiniteScroll
                    dataLength={loadList.length}
                    next={fetchMoreData}
                    hasMore={true}
                    loader={''}
                >
                    {loadList.map((item, index) =>
                        <Post key={index} path={item.avatar} bgi={item.img} name={item.name} postId={index}
                            com={ comList.map((elem, id) => (index == elem.class) ?
                                <Comment key={id} img={elem.avatar} txt={elem.text} name={elem.name} /> : '' )}/>)}
                </InfiniteScroll>
            </div>

            <div className={"users-wrapper"} data-testid="users-wrapper">
                <Followers  data-testid="descendant1"/>
                <Subscribers  data-testid="descendant2"/>
            </div>



        </div>
    );
};

const mapStateToProps = (state) => ({
    posts: selectPostsList(state),
    comments: selectCommentsList(state)
});


export default connect(mapStateToProps, {getPosts, getComments})(MainPage);