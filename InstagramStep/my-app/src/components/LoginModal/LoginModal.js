import React, {useEffect, useRef, useState} from 'react';
import"./LoginModal.scss";

const LoginModal = (props) => {
    const mail = useRef(null);
    const pass = useRef(null);
    const [prompt, setPrompt] = useState(true);
    const [login, setLog] = useState(false);

    useEffect(() => {
        if (localStorage.getItem('LoginInstagram') === 'true') {
            setLog(true);
        }
    }, [prompt]);


    return (
        <div className={`login-modal ${login ? 'dn' : ''}`}>
            <div className="login-action">
                <h3 className="login-action__header">Instagram</h3>
                <input ref={mail} type="email" className="login-action__mail" placeholder="Enter your mail"/>
                <input ref={pass} type="password" className="login-action__pass" placeholder="Enter your password"/>
                <button type="submit" className="login-action__btn" onClick={Login}>Log in</button>
                <p className={`login-action__err ${prompt ? 'dn' : ''}`}>Email or password incorrect!</p>
            </div>

            <p>Get the App.</p>

            <div className={"photo-btn"}>
                <img className={"p-btn apple-btn"} src="./img/LoginModal/app-store.png" alt="Apple-store" onClick={openAppStore}/>
                <img className={"p-btn google-btn"} src="./img/LoginModal/g-play.png" alt="Google-store" onClick={openGoogleStore}/>
            </div>
        </div>
    );

    function openAppStore() {
        window.open('https://apps.apple.com/app/instagram/id389801252?vt=lo', '_blank');
    }

    function openGoogleStore() {
        window.open('https://play.google.com/store/apps/details?id=com.instagram.android&referrer=utm_source%3Dinstagramweb%26utm_campaign%3DloginPage%26ig_mid%3DE4AE78AA-808E-4235-9B96-F3C210DDFED5%26utm_content%3Dlo%26utm_medium%3Dbadge', '_blank');
    }



    function Login() {
        if (mail.current.value === "horkovenko.k@gmail.com" && pass.current.value === "1") {
            setPrompt(true);
            setLog(true);
            localStorage.setItem('LoginInstagram', 'true');
        } else {
            setPrompt(false);
        }
    }

};



export default LoginModal;