import React from 'react';
import MainPage from "./components/MainPage/MainPage";
import UserPostsPage from "./components/UserPostsPage/UserPostsPage";
import RoutesMain from "./components/routes/RoutesMain";
import "./main.scss";

const App = () => {
    return (
        <div>
            <RoutesMain/>
        </div>
    );
};

export default App;